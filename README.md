# Sharelatex-full

A Docker image of Sharelatex with all packages preinstalled.
In the image https://hub.docker.com/r/sharelatex/sharelatex will just be all latex packages installed 
all credits go to overleaf https://github.com/overleaf/overleaf
